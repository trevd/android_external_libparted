# Copyright 2010 The Android Open Source Project

LOCAL_PATH:= $(call my-dir)

libfatfs_src_files := \
			fs/fat/bootsector.c \
			fs/fat/calc.c \
			fs/fat/clstdup.c \
			fs/fat/context.c \
			fs/fat/count.c \
			fs/fat/fat.c \
			fs/fat/fatio.c \
			fs/fat/resize.c \
			fs/fat/table.c \
			fs/fat/traverse.c \


libparted_src_files := \
			architecture.c \
			debug.c \
			device.c \
			disk.c \
			exception.c \
			filesys.c \
			libparted.c \
			timer.c \
			unit.c \
			dirname.c \
			basename.c \
			arch/linux.c \
			cs/natmath.c \
			cs/geom.c \
			cs/constraint.c \
			
libext2fs_src_files := \
		fs/ext2/ext2.c \
		fs/ext2/ext2_block_relocator.c \
		fs/ext2/ext2_buffer.c \
		fs/ext2/ext2_inode_relocator.c \
		fs/ext2/ext2_meta.c \
		fs/ext2/ext2_mkfs.c \
		fs/ext2/ext2_resize.c \
		fs/ext2/interface.c \
		fs/ext2/parted_io.c \
		fs/ext2/tune.c
		


liblabels_src_files := \
    labels/aix.c \
    labels/bsd.c \
    labels/dos.c \
    labels/dvh.c \
    labels/efi_crc32.c \
    labels/loop.c \
    labels/gpt.c \
    labels/mac.c \
    labels/pc98.c \
    labels/pt-limit.c \
    labels/pt-tools.c \
    labels/rdb.c \
    labels/vtoc.c \

uuid_dir := ../e2fsprogs/lib/uuid

libext2_uuid_src_files := \
	$(uuid_dir)/clear.c \
	$(uuid_dir)/compare.c \
	$(uuid_dir)/copy.c \
	$(uuid_dir)/isnull.c \
	$(uuid_dir)/pack.c \
	$(uuid_dir)/parse.c \
	$(uuid_dir)/unpack.c \
	$(uuid_dir)/unparse.c \
	$(uuid_dir)/uuid_time.c


libext2_uuid_cflags := -O2 -g \
	-DHAVE_INTTYPES_H \
	-DHAVE_UNISTD_H \
	-DHAVE_ERRNO_H \
	-DHAVE_NETINET_IN_H \
	-DHAVE_SYS_IOCTL_H \
	-DHAVE_SYS_MMAN_H \
	-DHAVE_SYS_MOUNT_H \
	-DHAVE_SYS_PRCTL_H \
	-DHAVE_SYS_RESOURCE_H \
	-DHAVE_SYS_SELECT_H \
	-DHAVE_SYS_STAT_H \
	-DHAVE_SYS_TYPES_H \
	-DHAVE_STDLIB_H \
	-DHAVE_STRDUP \
	-DHAVE_MMAP \
	-DHAVE_UTIME_H \
	-DHAVE_GETPAGESIZE \
	-DHAVE_LSEEK64 \
	-DHAVE_LSEEK64_PROTOTYPE \
	-DHAVE_EXT2_IOCTLS \
	-DHAVE_LINUX_FD_H \
	-DHAVE_TYPE_SSIZE_T \
	-DHAVE_SYS_TIME_H \
        -DHAVE_SYS_PARAM_H \
	-DHAVE_SYSCONF \
    


libparted_include_files := \
            external/e2fsprogs/lib/uuid \
            external/e2fsprogs/lib \
            external/libparted/include \
            external/libparted \
            external/libparted/arch \
            external/libparted/fs/ext2 \
            external/libparted/fs/fat \
            external/libparted/labels \


src_files := $(libparted_src_files) \
					$(libfatfs_src_files) \
					$(libext2fs_src_files) \
					$(liblabels_src_files) \

		
include $(CLEAR_VARS)
LOCAL_CLANG := true
LOCAL_SRC_FILES :=  $(libext2_uuid_src_files)	$(src_files)		
LOCAL_MODULE := libparted_static
LOCAL_STATIC_LIBRARIES := libc
LOCAL_CFLAGS += $(libext2_uuid_cflags) -Wno-missing-field-initializers
LOCAL_C_INCLUDES += $(libparted_include_files)
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_CLANG := true
LOCAL_SRC_FILES := $(src_files)
LOCAL_MODULE := libparted
LOCAL_SHARED_LIBRARIES := libc libext2_uuid
LOCAL_C_INCLUDES += $(libparted_include_files)
LOCAL_CFLAGS += -Wno-missing-field-initializers
include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_SRC_FILES :=$(src_files) 
LOCAL_MODULE := libparted_static
LOCAL_CFLAGS += $(libext2_uuid_cflags) -Wno-missing-field-initializers
LOCAL_LDFLAGS += -lc -luuid
LOCAL_C_INCLUDES += $(libparted_include_files)
include $(BUILD_HOST_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(src_files)
LOCAL_MODULE := libparted
LOCAL_SHARED_LIBRARIES := libext2_uuid_host
LOCAL_C_INCLUDES += $(libparted_include_files)
LOCAL_CFLAGS += -Wno-missing-field-initializers
include $(BUILD_HOST_SHARED_LIBRARY)



