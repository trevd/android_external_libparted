# Copyright 2012 Trevor Drake 

LOCAL_PATH:= $(call my-dir)


##### Test List Partitions ####

include $(CLEAR_VARS)
LOCAL_CLANG := true
LOCAL_C_INCLUDES += external/libparted/include
LOCAL_SHARED_LIBRARIES += libparted
LOCAL_SRC_FILES := list_partitions.c
LOCAL_MODULE := parted_list_partitions_test
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES += external/libparted/include
LOCAL_STATIC_LIBRARIES += libparted_static libc libm 
LOCAL_SRC_FILES := list_partitions.c
LOCAL_MODULE := parted_list_partitions_static_test
LOCAL_FORCE_STATIC_EXECUTABLE := true
include $(BUILD_EXECUTABLE)


include $(CLEAR_VARS)
LOCAL_C_INCLUDES += external/libparted/include
LOCAL_SHARED_LIBRARIES += libparted
LOCAL_SRC_FILES := list_partitions.c
LOCAL_MODULE := parted_list_partitions_test
include $(BUILD_HOST_EXECUTABLE)



##### Test List Partitions 1 ####

include $(CLEAR_VARS)
LOCAL_C_INCLUDES += external/libparted/include
LOCAL_SHARED_LIBRARIES += libparted
LOCAL_SRC_FILES := list_partitions1.c
LOCAL_MODULE := parted_list_partitions_test_1
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES += external/libparted/include
LOCAL_STATIC_LIBRARIES += libparted_static libc libm 
LOCAL_SRC_FILES := list_partitions1.c
LOCAL_MODULE := parted_list_partitions_static_test_1
LOCAL_FORCE_STATIC_EXECUTABLE := true
include $(BUILD_EXECUTABLE)


include $(CLEAR_VARS)
LOCAL_C_INCLUDES += external/libparted/include
LOCAL_SHARED_LIBRARIES += libparted
LOCAL_SRC_FILES := list_partitions1.c
LOCAL_MODULE := parted_list_partitions_test_1
include $(BUILD_HOST_EXECUTABLE)



##### Test Label ####

include $(CLEAR_VARS)
LOCAL_C_INCLUDES += external/libparted/include
LOCAL_SHARED_LIBRARIES += libparted
LOCAL_SRC_FILES := label.c
LOCAL_MODULE := parted_label_test
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES += external/libparted/include
LOCAL_STATIC_LIBRARIES += libparted_static libc libm 
LOCAL_SRC_FILES := label.c
LOCAL_MODULE := parted_label_static_test
LOCAL_FORCE_STATIC_EXECUTABLE := true
include $(BUILD_EXECUTABLE)


include $(CLEAR_VARS)
LOCAL_C_INCLUDES += external/libparted/include
LOCAL_SHARED_LIBRARIES += libparted
LOCAL_SRC_FILES := label.c
LOCAL_MODULE := parted_label_test



##### Test Reading ####

include $(CLEAR_VARS)
LOCAL_C_INCLUDES += external/libparted/include
LOCAL_SHARED_LIBRARIES += libparted
LOCAL_SRC_FILES := reading.c
LOCAL_MODULE := parted_reading_test
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES += external/libparted/include
LOCAL_STATIC_LIBRARIES += libparted_static libc libm 
LOCAL_SRC_FILES := reading.c
LOCAL_MODULE := parted_reading_static_test
LOCAL_FORCE_STATIC_EXECUTABLE := true
include $(BUILD_EXECUTABLE)


include $(CLEAR_VARS)
LOCAL_C_INCLUDES += external/libparted/include
LOCAL_SHARED_LIBRARIES += libparted
LOCAL_SRC_FILES := reading.c
LOCAL_MODULE := parted_reading_test



##### Test probe ####

include $(CLEAR_VARS)
LOCAL_C_INCLUDES += external/libparted/include
LOCAL_SHARED_LIBRARIES += libparted
LOCAL_SRC_FILES := probe.c
LOCAL_MODULE := parted_probe_test
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES += external/libparted/include
LOCAL_STATIC_LIBRARIES += libparted_static libc libm 
LOCAL_SRC_FILES := probe.c
LOCAL_MODULE := parted_probe_static_test
LOCAL_FORCE_STATIC_EXECUTABLE := true
include $(BUILD_EXECUTABLE)


include $(CLEAR_VARS)
LOCAL_C_INCLUDES += external/libparted/include
LOCAL_SHARED_LIBRARIES += libparted
LOCAL_SRC_FILES := probe.c
LOCAL_MODULE := parted_probe_test
