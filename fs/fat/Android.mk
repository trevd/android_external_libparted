# Copyright 2010 The Android Open Source Project

LOCAL_PATH:= $(call my-dir)

libfatfs_src_files := \
			bootsector.c \
			calc.c \
			clstdup.c \
			context.c \
			count.c \
			fat.c \
			fatio.c \
			resize.c \
			table.c \
			traverse.c \

		
		
include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(libfatfs_src_files)
LOCAL_MODULE := libfatfs
LOCAL_C_INCLUDES += external/e2fsprogs/lib \
					external/parted/include
include $(BUILD_STATIC_LIBRARY)
