# Copyright 2010 The Android Open Source Project

LOCAL_PATH:= $(call my-dir)

libext2fs_src_files := \
		ext2.c \
		ext2_block_relocator.c \
		ext2_buffer.c \
		ext2_inode_relocator.c \
		ext2_meta.c \
		ext2_mkfs.c \
		ext2_resize.c \
		interface.c \
		parted_io.c \
		tune.c
		
		
include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(libext2fs_src_files)
LOCAL_MODULE := libext2fs
LOCAL_C_INCLUDES += external/e2fsprogs/lib \
					external/parted/include
include $(BUILD_STATIC_LIBRARY)
